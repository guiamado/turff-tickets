export default {
  methods: {
    paginate (list, size) {
      return list.reduce((acc, value, i) => {
        const index = Math.floor(i / size)
        const page = acc[index] || (acc[index] = [])
        page.push(value)

        return acc
      }, [])
    }
  }
}
