import { index } from './index'

function getTickets () {
  const url = '/tickets'
  return index.get(`${url}`)
}

function getTicketById (id) {
  const url = '/tickets/'
  return index.get(`${url}${id}`)
}

function editTicketById (id) {
  const url = '/tickets/edit/'
  return index.get(`${url}${id}`)
}

function createTicket () {
  const url = '/tickets/create'
  return index.get(`${url}`)
}

export const ticket = {
  getTickets,
  getTicketById,
  editTicketById,
  createTicket
}
