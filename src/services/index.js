import axios from 'axios'

function get (url) {
  return axios.get(`${process.env.VUE_APP_API}${url}`)
}

function post (url, data) {
  return axios.post(`${process.env.VUE_APP_API}${url}`, data)
}

export const index = {
  get,
  post
}
